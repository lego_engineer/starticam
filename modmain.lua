-- Modmain
-- [[ This mod just sets the camera rotation at start for each player of a server. If the camera is not set to the specified angle, it will set it to the specified angle.
-- ]]

local function FollowCameraPostInit(self)
	-- Save the original default function
    local FollowCameraSetDefault = self.SetDefault
	
    self.SetDefault = function(self)
		-- Run the original default function
        FollowCameraSetDefault(self)
		
		-- If not the set value, then set it to that value. 
		if self.headingtarget ~= GetModConfigData("camera_default_angle") then
			self.headingtarget = GetModConfigData("camera_default_angle")
		end

	end
end

AddClassPostConstruct("cameras/followcamera", FollowCameraPostInit)