[![forthebadge](https://forthebadge.com/images/badges/built-with-science.svg)](https://forthebadge.com)

# Starticam

Don't Starve Together mod to set the world's default camera angle.

###### Available at
Gitlab: https://gitlab.com/lego_engineer/starticam

Steam: https://steamcommunity.com/sharedfiles/filedetails/?id=1515116066

Klei: https://forums.kleientertainment.com/files/file/1883-starticam/