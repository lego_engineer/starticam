name = "Starticam"
description = [[Set the default angle for your server's camera.

When starting a new world, or reloading an old one, it can often be annoying to need to shift the camera to your favorite camera angle and coordinate this with your teammates. This can especially be a nuisance for those who like to play with the camera squared up to the world grid with teammates who do not like to shift their camera after they start.

With this mod, the camera default camera angle for the server may be specified in the configuration, thus streamlining this process. Upon entering the world, the camera will be set to the specified angle, after which time it operate as normal
]]
author = "lego_engineer"
version = "1.0.1"
forumthread = "/files/file/1883-starticam/"

--[[
CHANGELOG:
1.0.1 - Update files available at links

Files Available at:
 Klei: https://forums.kleientertainment.com/files/file/1883-starticam/
 Gitlab: https://gitlab.com/lego_engineer/starticam
 Steam: https://steamcommunity.com/sharedfiles/filedetails/?id=1515116066
]]


api_version_dst = 10
dont_starve_compatible = false
reign_of_giants_compatible = false
dst_compatible = true
all_clients_require_mod = true 

icon_atlas = "modicon.xml"
icon = "modicon.tex"

server_filter_tags = {"camera"}

configuration_options =
{

	{
		name="camera_default_angle",
		label="Server's Default Camera Angle",
		hover = "The default camera angle in degrees.",
		options = {
			{data=0, description="0 (Square)"},
			{data=45, description="45 (Default)"},
			{data=90, description="90 (Square)"},
			{data=135, description="135 (Diamond)"},
			{data=180, description="180 (Square)"},
			{data=225, description="225 (Diamond"},
			{data=270, description="270 (Square)"},
			{data=315, description="315 (Diamond"},
		},
		default=45
	},
	
}